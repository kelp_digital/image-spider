select
    count(*)
from
    public.images
where
    fixed = false
    and rest_attributes ?| array['data-src',
    'data-lazy',
    'data-original',
    'data-cid',
    'data-srcset',
    'data-lazy-srcset'];

select
    count(distinct src_hash)
from
    public.images as i ;

select
    count(*)
from
    public.images
where
    processed = false
    and fixed = true 
;
-- how many images have any metadata
 select
count(distinct i.id)
-- i.*
from
    public.images as i
left join public.metadata as m on
    m.image_id = i.id
where
    m.id is not null
    and i.processed = true;


select
    *
from
    public.images
where
    fixed = false;

select
    count(distinct src_hash)
from
    public.images
order by
    count desc;
-- REPORTS

-- COPYRIGHT reports
 select
    x.uri,
    --	x.value,
 count(r.id)
from
    public.images r
left join public.metadata x on
    r.id = x.image_id
where
    x.uri is null
group by
    x.uri ;
-- unprocessed images
 select
    count(id) as unprocessed
    --	,*

    from public.images
where
    processed = false;
--- metadata namespace over unique images distribution
 select
    count(distinct image_id) as vals ,
    namespace as labels
from
    public.metadata
group by
    namespace;
-- how many unique images has at least one metadata
 select
    count(distinct image_id)
from
    public.metadata;
--- file server server usage distribution
 select
    count(id),
    server
from
    public.images
group by
    server;
--- copyright distribution
 select
    distinct(x.uri) ,
    count(x.id)
from
    public.metadata x
left join public.images r on
    r.id = x.image_id
where
    x.uri like '%copyright%'
    and x.uri not like '%icc:%'
    or x.uri like '%rights%'
group by
    x.uri
order by
    x.uri asc;



-- few people has this kind of setup, check it out
 select
    *
from
    public.metadata
where
    uri = 'iptc:copyright:0';
-- misc
 select
    *
from
    public.images
where
processed=false
;
-- duplicates
 select
    count(*)
from
    (
    select
        count(id) over (partition by src_hash) as dupesCount,
        *
    from
        public.images
    where
        processed = false ) as dupes
where
    dupes.dupesCount > 1;
----- DELETE DUPES
 delete
from
    public.images
where
    id in (
    select
        id
    from
        (
        select
            count(id) over (partition by src_hash) as dupesCount,
            *
        from
            public.images
        where
            processed = false ) as dupes
    where
        dupes.dupesCount > 1 );

    
    
delete
from
    public.images
where
    src like '%.gif';

select
    count(*)
from
    public.images
where
    src like '%.gif';
    

----- DELETE DUPES
 delete
from
    public.images
where
    id in (
    select
        id
    from
        (
        select
            count(id) over (partition by src_hash) as dupesCount,
            *
        from
            public.images
        where
            processed = true ) as dupes
    where
        dupes.dupesCount > 1 );
-- duplicates
 select
    count(*)
from
    (
    select
        count(id) over (partition by src_hash) as dupesCount,
        *
    from
        public.images
    where
        processed = true ) as dupes
where
    dupes.dupesCount > 1;
    

delete from public.metadata as m where m.uri like 'exif:exif:UserComment:%';