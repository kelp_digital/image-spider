select
    (
    select
        count( distinct ui.id) imgs
    from
        public.images as ui
    left join public.metadata as m on
        m.image_id = ui.id
    where
        m.id is null ) as imgs_no_meta, (select
        count( distinct ui.id) imgs
    from
        public.images as ui
    left join public.metadata as m on
        m.image_id = ui.id
    where
        m.id is not null) as imgs_with_meta ;
--------------------------------
--------------------------------
--------------------------------
--------------------------------
--------------------------------

--- copyright distribution
 select
    --    distinct(m.uri) as copyright,
 count(distinct m.image_id) as imgs,
    u.market_type,
    ic."count" as total_in_category,
    (count(distinct m.image_id)::decimal / ic."count")* 100 as pct
from
    public.metadata as m
left join public.unique_images as i on
    i.id = m.image_id
join public.urls as u on
    u.id = i.url_id
join public.images_per_category as ic on
    ic.market_type = u.market_type
group by
    m.uri,
    u.market_type,
    ic."count"
having
    imgs > 1 ;

select
    u.market_type
from
    public.urls as u
group by
    u.market_type;
----------------------
----------------------
----------------------

--- file server server usage distribution
 select
    count(id),
    server
from
    public.images
group by
    server;
----------------------
----------------------
----------------------

--- metadata namespace over unique images distribution
 select
    count(distinct image_id) as vals ,
    namespace as labels
from
    public.metadata
group by
    namespace;
----------------------
----------------------
----------------------

-- how many images has at least one metadata
 select
    count(distinct m.image_id)
from
    public.unique_images as i
join public.metadata as m on
    i.id = m.image_id
    --    group by image_id
;
----------------------
----------------------
----------------------

-- total images
 select
    (
    select
        count(*)
    from
        public.images as i) as totalImages,
    (
    select
        count(id)
    from
        public.unique_images as ui) as uniqueImages;
----------------------
----------------------
----------------------289803

-- metadata namespace distribution
 select
    count(i.id) countImages,
    m."namespace"
from
    public.unique_images as i
join public.metadata as m on
    m.image_id = i.id
group by
    m."namespace";

select
    count(id)
from
    public.unique_images as ui;