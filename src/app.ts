require('dotenv-extended').load()
const Sentry = require('@sentry/node')
Sentry.init({
  dsn: 'https://4f33de31988941b98461cd6b63504bee@sentry.io/1773579'
})

import { db } from './utils/db'

import { retrieveImagesAndCreateJobs } from './jobs/helperFunctions'

import { setupRedis } from './utils/redis'
import { elapsed_time } from './utils/utils'

var log = require('debug')('sen:app')
var logError = require('debug')('sen:app:error')

async function main() {
  try {
    const start = process.hrtime()
    await db()
    await setupRedis()

    await Promise.all([
      // await startWithNotProcessedURLs(),
      await retrieveImagesAndCreateJobs()
      // await fixImagesSrc()
    ])

    log(
      `DONE --- it took ${elapsed_time(
        start
      )}. Now wait until all the jobs are done`
    )
  } catch (error) {
    logError(error.message)
  }
}
main()
