// tslint:disable

import inquirer from 'inquirer'
import { readParseAndSync, fileExists } from './utils/csv'
import { isURL } from 'validator'

import { db } from './utils/db'
import { addUrl } from './models/url'
import { fixImagesSrc } from './jobs/helperFunctions'

var log = require('debug')('sen:cli')
var logError = require('debug')('sen:cli:error')

async function main() {
  var questions = [
    {
      type: 'list',
      name: 'action',
      message: 'Please choose the action',
      choices: ['addUrl', 'syncCsv', 'startCrawler', 'fixImagesSrc']
    },
    {
      type: 'input',
      name: 'csvFile',
      message: 'Please provide path to the csv file',
      default: 'list.csv',
      when: answers => {
        return answers.action === 'syncCsv'
      },
      validate: (input: string) => {
        if (fileExists(input)) {
          return true
        } else {
          return `File doesn't exist. Try again. :) `
        }
      }
    },
    {
      type: 'input',
      name: 'addUrl:url',
      message: 'What is the URL',
      when: answers => {
        return answers.action === 'addUrl'
      },
      validate: (input: string) => {
        if (isURL(input)) {
          return true
        } else {
          return `Not valid URL :) `
        }
      }
    },
    {
      type: 'list',
      name: 'addUrl:marketType',
      message: 'Add market type',
      choices: [
        'stockPhoto',
        'mediaGlobalMainstream',
        'mediaLocalMainstream',
        'mediaGlobalVertical',
        'mediaLocalVertical',
        'photographyPortfolio'
      ],
      when: answers => {
        return answers.action === 'addUrl'
      }
    }
  ]
  let done = false
  const answers: any = await inquirer.prompt(questions).catch(e => log(e))

  // set up the database connection
  await db()

  /// ACTION
  if (answers.action === 'syncCsv') {
    log('Running syncCSV')
    await readParseAndSync(answers.csvFile)
    done = true
  } else if (answers.action === 'addUrl') {
    log('Running addURL')
    await addUrl({
      url: answers['addUrl:url'],
      marketType: answers['addUrl:marketType']
    })
    done = true
  } else if (answers.action === 'startCrawler') {
    log('clear ; $env:DEBUG="sen:*"; yarn crawler:start')
    done = true
  } else if (answers.action === 'fixImagesSrc') {
    await fixImagesSrc()
    log('done')
    done = true
  }

  setInterval(function() {
    if (done) {
      process.stdout.write('\n')
      process.exit(0)
    }
  }, 1000)
}
main()
