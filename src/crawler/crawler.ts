import cheerio from 'cheerio'
import axios from 'axios'
import { UrlModel } from '../models/url'
import multihashing from 'multihashing-async'
import { urlQ } from '../jobs/webpage'
import { URL } from 'url'
import { imagesSaveQ } from '../jobs/image'
import { hasExtension, makeHash } from '../utils/utils'
import { Op } from 'sequelize'

var log = require('debug')('sen:crawler')
var logInfo = require('debug')('sen:crawler:info')

/**
 * Download url and create Cheerio instance
 * @param uri
 */
export async function createCheerioInstance(uri) {
  try {
    const resp = await axios.get(uri, {
      headers: {
        'User-Agent': 'piexie reverse image search 0.1.0'
      }
    })
    return cheerio.load(resp.data)
  } catch (error) {
    return Promise.reject(error.response.data)
  }
}

export async function visitPage(
  uri: string,
  shouldCollectLinks = true,
  marketType: string
) {
  const { href, protocol, host } = new URL(uri)

  const $ = await createCheerioInstance(href)

  const urlHashString = await makeHash(href.trim())

  log(`Processing \n page: ${href} \n hash: ${urlHashString}`)

  const [model] = await UrlModel().findOrCreate({
    where: {
      urlHash: urlHashString
    },
    defaults: {
      url: href.trim(),
      marketType
    }
  })

  const modelId = model.get('id')

  imagesSaveQ.add(
    { images: collectImages($), urlId: modelId, uri: href },
    { removeOnComplete: true }
  )

  if (shouldCollectLinks) {
    const links = collectLinks($)
    if (links.internal) {
      await Promise.all(
        links.internal.map(async v => {
          if (v != '/') {
            const pageToVisit = [protocol, host].join('//') + v
            // logInfo('adding page to visit ', pageToVisit)
            return await urlQ.add(
              {
                uri: pageToVisit,
                collectLinks: shouldCollectLinks,
                marketType
              },
              { removeOnComplete: true }
            )
          }
          return Promise.resolve()
        })
      )
    }
  }
  return await model.update({
    processed: true
  })
}

export function collectImages($): any[] {
  let images = []
  $('img').each((_, el: never) => {
    const { attribs } = el

    if (attribs['width'] <= 1) {
      return
    }
    if (
      attribs['src'] ===
      'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7'
    ) {
      return
    }
    images.push(attribs)
  })

  log(`Found ${images.length} images`)
  return images
}

export function collectLinks($) {
  let internal: string[] = []
  let external: string[] = []

  let relativeLinks = $("a[href^='/']")
  relativeLinks.each(function() {
    const href = $(this).attr('href')
    if (!hasExtension(href)) {
      internal.push(href)
    }
  })

  let absoluteLinks = $("a[href^='http']")
  absoluteLinks.each(function() {
    const href = $(this).attr('href')
    if (!hasExtension(href)) {
      external.push(href)
    }
  })

  // dedupe
  internal = [...new Set(internal)]
  external = [...new Set(external)]

  log('Found ' + internal.length + ' internal links')
  // log('Found ' + external.length + ' external links')
  return { external, internal }
}

/**
 * Start with predefined urls
 */
export async function startWithNotProcessedURLs(rediscover = false) {
  const list = await UrlModel().findAll({
    // limit: 1,
    where: {
      processed: rediscover
      // url: { [Op.like]: '%buzz%' }
    }
  })
  return await Promise.all(
    list.map(async l => {
      return await urlQ.add({
        uri: l.get('url'),
        marketType: l.get('marketType')
        // collectLinks: false
      })
    })
  )
}
