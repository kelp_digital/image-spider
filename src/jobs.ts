require('dotenv-extended').load()
const Sentry = require('@sentry/node')
Sentry.init({
  dsn: 'https://4f33de31988941b98461cd6b63504bee@sentry.io/1773579'
})

import { db } from './utils/db'

import { setupRedis } from './utils/redis'
import { elapsed_time } from './utils/utils'

import { setupJobs } from './jobs/index'

var log = require('debug')('sen:jobs')
var logError = require('debug')('sen:jobs:error')

async function main() {
  try {
    const start = process.hrtime()
    await db()
    await setupRedis()
    await setupJobs()

    log(`JOB QUEUES ARE RUNNING ...`)
  } catch (error) {
    logError(error.message)
  }
}
main()
