import { imageQ, imagesSaveQ, updateImageQ } from './image'
import { ImageModel, Image } from '../models/image'
import axios from 'axios'
import sharp from 'sharp'
import { parse } from 'icc'
import { fromBuffer } from 'xmp-reader'
import iptcReader from 'iptc-reader'
import exifReader from 'exif-reader'
import { phash } from '../phash/phash'
import multihashing from 'multihashing-async'
import { Op } from 'sequelize'
import { makeHash, fixUrl } from '../utils/utils'
import URL from 'url-parse'
import { getFromCache, setToCache } from '../utils/redis'

import {
  MetadataModel,
  generateMetadataTableBulkInsertRecords
} from '../models/metadata'
import { c } from '../utils/db'

var log = require('debug')('sen:image')
var logInfo = require('debug')('sen:image:info')

const LIMIT = 10000
const ignoredDomains = [
  'insightexpressai',
  'cdn.shortpixel.ai',
  'track.adform.net'
]
const ignoredDomainsRegex = RegExp(ignoredDomains.join('|'), 'igm')

export async function retrieveImagesAndCreateJobs() {
  const where = {
    processed: false
    // fixed: true
  }
  const count = await ImageModel().count({
    where

    // : {
    //   // width: { [Op.ne]: 1 },
    //   processed: false,
    //   // errors: null,
    //   fixed: true
    //   // content_hash: null
    // }
  })

  log(`Images to process ${count}`)

  if (count === 0) {
    logInfo(`Nothing to do, exiting ...`)
    return Promise.resolve()
  }

  const ratio = Math.round(count / LIMIT)
  let amountOfIterations = ratio >= 1 ? ratio : 1
  // amountOfIterations = 1
  logInfo(`Looping through ${amountOfIterations} times with Limit ${LIMIT}`)

  let offset = 0
  const delay = ms => new Promise(resolve => setTimeout(resolve, ms))

  for (let i = 0; i < amountOfIterations; i++) {
    logInfo(`Progress done ${((i / amountOfIterations) * 100).toFixed(4)}%`)
    const all = await ImageModel().findAll({
      // attributes: [
      //   [Sequelize.fn('DISTINCT', Sequelize.col('src_hash')), 'src_hash'],
      //   '*'
      // ],
      where,
      offset,
      limit: LIMIT
    })

    offset += LIMIT

    await Promise.all(
      all.map(async img => {
        // logInfo(`Adding job for imageID ${img.get('id')}`)
        return await imageQ.add(
          { id: img.get('id'), src: img.get('src'), ...img.get() }
          // { removeOnComplete: true }
        )
      })
    )
    await delay(6000)
  }
}

async function downloadImage(src: string) {
  try {
    const resp = await axios.get(src, { responseType: 'arraybuffer' })
    return resp
  } catch (error) {
    return Promise.reject(error.response.data)
  }
}

interface ImageMetadataDB {
  tags: { [k: string]: any }
  image: { [k: string]: any }
}
export async function imageMetadata(src: string): Promise<ImageMetadataDB> {
  if (src.startsWith('http')) {
    try {
      const {
        data,
        headers: { server }
      } = await downloadImage(src)

      const sharpedImage = sharp(data)
      // skip metadata for svg
      const { exif, iptc, icc, xmp, ...rest } = await sharpedImage.metadata()
      const iccProfile = icc ? parse(icc) : null
      const xmpTags = xmp ? await fromBuffer(xmp) : null
      const exifTags = exif ? exifReader(exif) : null
      const iptcTags = iptc ? iptcReader(iptc) : null
      const contentHash = await multihashing(
        await sharpedImage.toBuffer(),
        'sha2-256'
      )
      const metadataHash = await multihashing(
        Buffer.from(
          JSON.stringify({
            icc: iccProfile,
            xmp: xmpTags,
            exif: exifTags,
            iptc: iptcTags
          })
        ),
        'sha2-256'
      )

      return Promise.resolve({
        tags: {
          icc: iccProfile,
          xmp: xmpTags,
          exif: exifTags,
          iptc: iptcTags
        },
        image: {
          ...rest,
          phash: await phash(sharpedImage),
          content_hash: contentHash.toString('hex'),
          metadata_hash: metadataHash.toString('hex'),
          server
        }
      })
    } catch (error) {
      return Promise.reject(error)
    }
  } else if (src.startsWith('data:image')) {
    return Promise.reject(new Error('not implemented'))
  } else {
    return Promise.reject(
      new Error('is not url nor data scheme, prob starts with /')
    )
  }
}

/**
 * Main processing and transformation of the images
 */
export async function processImagesMetadata(job) {
  try {
    const { id, src } = job.data
    let realSrc = src

    const srcHash = await makeHash(src)
    const cached = await getFromCache(srcHash, 'imageQ')
    if (cached) {
      if (cached == id) {
        throw new Error(
          `cached ID and processing id are the same ${cached} - ${id}`
        )
      }
      // find all the duplicates and copy the data excluding the url_id
      await ImageModel().destroy({
        where: {
          id: id
        }
      })
      return Promise.resolve({
        id,
        message: `Image deleted, suspected duplicate`
      })
    }

    const model = await ImageModel().findOne({
      where: {
        id
      }
    })

    if (!model) {
      return Promise.reject(
        new Error(`image doesn't exist srcHash: ${srcHash}`)
      )
    }

    const metadata = await imageMetadata(realSrc)

    await ImageModel().update(
      {
        processed: true,
        src: realSrc,
        srcHash: srcHash,
        ...metadata.image
      },
      { where: { id } }
    )

    const bulkMetadataRecords = generateMetadataTableBulkInsertRecords(
      metadata.tags,
      id
    )
    await MetadataModel()
      .bulkCreate(bulkMetadataRecords, {
        ignoreDuplicates: true
      })
      .catch(e => {
        throw new Error(e.message)
      })

    await setToCache(srcHash, id, 'imageQ')
    return Promise.resolve({
      id,
      processed: true,
      updated_at: model.get('updatedAt')
    })
  } catch (error) {
    return Promise.reject(error)
  }
}

export async function saveImagesToDB(job) {
  try {
    const { images, urlId, uri } = job.data
    const { hostname, protocol, ...theRest } = new URL(uri)

    log(`Processing ${images.length} images for urlID ${urlId}`)
    const d: object[] = await Promise.all(
      images.map(async i => {
        const baseHostname = [protocol, hostname].join('//')
        const { src, alt, sizes, width, height, id, ...restAttributes } = i
        const { src: realSrc, srcSet: realSrcSet } = findCorrectSrcAndSrcSet(
          i,
          baseHostname
        )
        const originalSrcUrl = new URL(fixUrl(src), baseHostname)
        const originalSrc = originalSrcUrl.toString()
        // no idea why i need to do this twice :D
        if (ignoredDomainsRegex.test(realSrc)) {
          log(`Skipping url because it is in ignored list`)
          return null
        }

        return {
          ...restAttributes,
          alt,
          originalSrc,
          src: realSrc,
          srcHash: await makeHash(realSrc),
          srcSet: realSrcSet,
          sizes,
          width: width ? parseInt(width, 10) : null,
          height: height ? parseInt(height, 10) : null,
          urlId
        }
      })
    )

    // save
    const s = await ImageModel().bulkCreate(d.filter(Boolean), {
      ignoreDuplicates: true
    })

    return Promise.resolve({ imagesSaved: s.length })
  } catch (error) {
    return Promise.reject(error)
  }
}

export async function updateImage(job) {
  try {
    const { image, uri } = job.data
    const { id, ...rest } = image

    // log(`Processing imageID ${id}`)
    let baseHostname: string | null = null
    if (uri) {
      const { hostname, protocol } = new URL(uri)
      baseHostname = [protocol, hostname].join('//')
    }

    const { src: realSrc, srcSet: realSrcSet } = findCorrectSrcAndSrcSet(
      image,
      baseHostname
    )

    // no idea why i need to do this twice :D
    if (ignoredDomainsRegex.test(realSrc)) {
      log(`Skipping url because it is in ignored list`)
      return Promise.resolve({ skipped: true })
    }

    const ret = {
      ...rest,
      src: realSrc,
      src_hash: await makeHash(realSrc),
      src_set: realSrcSet,
      fixed: true
    }

    await ImageModel().update(ret, { where: { id }, returning: true })
    return Promise.resolve({ id })
  } catch (error) {
    return Promise.reject(error)
  }
}

export function findCorrectSrcAndSrcSet(
  image: {
    [k: string]: any
  },
  baseHostname?: string | null
): { src: string; srcSet: string } {
  let realSrc: string
  let realSrcSet: string
  let restAttributes: string

  const {
    src,
    srcset: srcSet,
    rest_attributes,
    restAttributes: modelRestAttributes
  } = image
  if (rest_attributes) {
    restAttributes = rest_attributes
  } else {
    restAttributes = modelRestAttributes
  }

  if (!src) {
    throw new Error(`SRC is missing  ` + JSON.stringify(image))
  }
  // sometimes src is in other attributes, for example data-src, data-original
  realSrc = src
  realSrcSet = srcSet
  // lets check does this image has originals somewhere in data- attributes
  if (restAttributes['data-src']) {
    realSrc = restAttributes['data-src']
  } else if (restAttributes['data-original']) {
    realSrc = restAttributes['data-original']
  } else if (restAttributes['data-cid']) {
    realSrc = restAttributes['data-cid']
  } else if (restAttributes['data-lazy']) {
    realSrc = restAttributes['data-lazy']
  } else if (restAttributes['data-lazy-srcset']) {
    realSrcSet = restAttributes['data-lazy-srcset']
  }

  realSrc = fixUrl(realSrc)

  if (baseHostname) {
    const url = new URL(fixUrl(realSrc), baseHostname)
    realSrc = url.toString()
  }

  return {
    src: realSrc,
    srcSet: realSrcSet
  }
}

export async function fixImagesSrc(): Promise<any> {
  const [q] = await c.query(
    `SELECT count(*) FROM public.images 
    WHERE fixed is null and rest_attributes ?| array['data-src', 'data-lazy','data-original', 'data-cid', 'data-srcset', 'data-lazy-srcset']
    ;`
  )
  const { count } = q[0]

  log(`Images to process ${count}`)

  if (count === 0) {
    logInfo(`Nothing to do, exiting ...`)
    return Promise.resolve()
  }
  const ratio = Math.round(count / LIMIT)
  let amountOfIterations = ratio > 0 ? ratio : ratio + 1
  // const amountOfIterations = 4
  logInfo(`Looping through ${amountOfIterations} times with Limit ${LIMIT}`)
  // return
  let offset = 0

  for (let i = 0; i < amountOfIterations; i++) {
    // log(`Done ${(i / amountOfIterations) * 100}%`)
    const q = `select * from public.images where fixed is null and rest_attributes ?| array['data-src', 'data-lazy', 'data-original', 'data-cid', 'data-srcset', 'data-lazy-srcset'] offset ${offset} limit ${LIMIT};`

    offset += LIMIT

    const [all] = await c.query(q)
    await Promise.all(
      all.map(async image => {
        return await updateImageQ.add(
          { image }
          // { removeOnComplete: true }
        )
        // const d = await updateImage({ data: { image } })

        // log('done', image.id)
        // return d
      })
    )
    console.log('done with the batch', { offset, LIMIT })
  }
}
