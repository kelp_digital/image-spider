import { Sequelize, Op } from 'sequelize'
import Bull from 'bull'
import { ImageModel } from '../models/image'
import { redisClient } from '../utils/redis'
export const log = require('debug')('sen:jobs:imagesSaveQ')
export const logImageQ = require('debug')('sen:jobs:imageQ')
export const errorImageQ = require('debug')('sen:jobs:imageQ:error')

// Qs
export const imageQ = new Bull('imageQ', redisClient)
export const imagesSaveQ = new Bull('imagesSaveQ', redisClient)
export const updateImageQ = new Bull('updateImageQ', redisClient)

// imageQ.on('active', function(job, jobPromise) {
//   // A job has started. You can use `jobPromise.cancel()`` to abort it.
//   log(`Got ${job.name}`)
// })

imageQ.on('completed', (job, result) => {
  if (result['message']) {
    logImageQ(`Job completed ImageID: ${result.id} ${result['message']}`)
  }
})

imageQ.on('error', function(error) {
  // An error occurred

  errorImageQ(`Job error for  ${error.message}`)
})

imageQ.on('failed', async function(job, error) {
  const {
    data: { id }
  } = job
  let updateFields = {
    processed: true,
    errors: Sequelize.fn('array_append', Sequelize.col('errors'), error.message)
  }
  if (error.message === 'Invalid IPTC directory') {
    updateFields['invalid_iptc'] = true
  }
  await ImageModel().update(updateFields, {
    where: {
      id
    }
  })
  errorImageQ(`Job failed for ImageID: ${id}  ${error.message}`)
})

// imagesSaveQ.on('active', function(job, jobPromise) {
//   // A job has started. You can use `jobPromise.cancel()`` to abort it.
//   log(`Got ${job.name}`)
// })

// imagesSaveQ.on('completed', (job, result) => {
//   log(`Job completed. Images saved`, result.imagesSaved)
// })

imagesSaveQ.on('error', function(error) {
  // An error occurred
  errorImageQ('Job error ' + error.message)
})

imagesSaveQ.on('failed', function({ data: { id } }, error) {
  ImageModel()
    .update(
      {
        processed: true,
        errors: Sequelize.fn(
          'array_append',
          Sequelize.col('errors'),
          error.message
        )
      },
      {
        where: {
          id
        }
      }
    )
    .then(() => {
      log('done')
    })
  errorImageQ(`Job failed with error ${error.message}`)
})

updateImageQ.on('completed', (job, result) => {
  if (result['message']) {
    logImageQ(`Job completed ImageID: ${result.id} ${result['message']}`)
  }
})

updateImageQ.on('error', function(error) {
  // An error occurred
  errorImageQ('Job errored', error.message)
})

updateImageQ.on('failed', function({ data: { id } }, error) {
  ImageModel()
    .update(
      {
        processed: true,
        errors: Sequelize.fn(
          'array_append',
          Sequelize.col('errors'),
          error.message
        )
      },
      {
        where: {
          id
        }
      }
    )
    .then(() => {
      log('done')
    })
  errorImageQ(`Job failed with error ${error.message}`)
})
