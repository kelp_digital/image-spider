import { startCluster } from './startCluster'
import { imageQ, imagesSaveQ, updateImageQ } from './image'
import {
  processImagesMetadata,
  saveImagesToDB,
  updateImage
} from './helperFunctions'
import { urlQ, processURLQueue } from './webpage'

const concurrency = 100

export async function setupJobs() {
  return await startCluster([
    urlQ.process(concurrency, async job => processURLQueue(job)),
    imageQ.process(concurrency, async job => processImagesMetadata(job)),
    updateImageQ.process(concurrency, async job => updateImage(job)),
    imagesSaveQ.process(concurrency, async job => saveImagesToDB(job))
  ])
}
