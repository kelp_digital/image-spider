import cluster from 'cluster'
var log = require('debug')('sen:cluster')

let numCPUs = require('os').cpus().length - 10

// numCPUs = 2

export async function startCluster(queues: Promise<void>[]) {
  if (cluster.isMaster) {
    log(`Master ${process.pid} is running`)

    for (var i = 0; i < numCPUs; i++) {
      cluster.fork()
      log(`Forking ${i + 1}`)
    }

    cluster.on('online', async function(worker) {
      log(`Cluster online, ${worker.id}`)
    })

    cluster.on('exit', function(worker, code, signal) {
      log('worker ' + worker.process.pid + ' died')
    })
  } else {
    log(`Worker ${process.pid} started`)
    return await Promise.all(queues.map(async q => await q))
  }
}
