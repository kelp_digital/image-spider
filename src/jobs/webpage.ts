import Bull from 'bull'
import { Sequelize } from 'sequelize'
import { isURL } from 'validator'
import { redisClient, getFromCache, setToCache } from '../utils/redis'
import { makeHash } from '../utils/utils'
import { visitPage } from '../crawler/crawler'
import { UrlModel } from '../models/url'

const log = require('debug')('sen:jobs:urlQ')
const logError = require('debug')('sen:jobs:urlQ:error')

export const urlQ = new Bull('urlQ', redisClient)

// urlQ.on('active', function(job, jobPromise) {
//   // A job has started. You can use `jobPromise.cancel()`` to abort it.
//   log(`Got ${job.name}`)
// })
urlQ.on('completed', (job, result) => {
  log(`Job completed for urlHash`, result.urlHash)
})
// // Local events pass the job instance...
// urlQ.on('progress', function(job, progress) {
//   log(`Job ${job.id} is ${progress}% ready!`)
// })
urlQ.on('error', function(error) {
  logError('Job error', error.message)
})
urlQ.on('failed', async function(job, error) {
  const {
    data: { uri }
  } = job
  const urlHashString = await makeHash(uri)
  let updateFields = {
    processed: true,
    errors: Sequelize.fn('array_append', Sequelize.col('errors'), error.message)
  }

  await UrlModel().update(updateFields, {
    where: {
      urlHash: urlHashString
    }
  })
  logError(`Job failed with uri ${uri} and error ${error.message}`, error)
})

export async function processURLQueue(job) {
  try {
    const { uri, collectLinks, marketType } = job.data

    const urlHashString = await makeHash(uri)

    if (await getFromCache(urlHashString)) {
      return Promise.resolve({ urlHash: urlHashString })
      // return Promise.reject(
      //   new Error(`Url exist with the hash ${urlHashString}`)
      // )
    }

    if (isURL(uri)) {
      log(`Starting to visit page ${uri}`)
      await visitPage(uri, collectLinks, marketType)
      await setToCache(urlHashString, 1)
      return Promise.resolve({ urlHash: urlHashString })
    }
  } catch (error) {
    return Promise.reject(error)
  }
}
