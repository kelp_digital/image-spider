import { dbSetup } from './utils/db'

async function main() {
  try {
    await dbSetup()
  } catch (error) {
    console.error(error)
  }
}

main()
