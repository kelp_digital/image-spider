require('dotenv-extended').load()
import { db } from './utils/db'

import { retrieveImagesAndCreateJobs } from './jobs/helperFunctions'
import { startWithNotProcessedURLs } from './crawler/crawler'

import { setupRedis } from './utils/redis'
import { migrateMetadata } from './models/metadata'
import { elapsed_time } from './utils/utils'
var log = require('debug')('sen:migratStructure')
var logError = require('debug')('sen:migratStructure:error')

async function main() {
  try {
    const start = process.hrtime()
    await db()

    await migrateMetadata()
    log(`DONE --- it took ${elapsed_time(start)} miliseconds`)
  } catch (error) {
    logError(error)
  }
}
main()
