import { Model, STRING, JSONB, BOOLEAN, TEXT, INTEGER, ARRAY } from 'sequelize'
import { c } from '../utils/db'
import { UrlModel } from './url'
import { fixUrl } from '../utils/utils'
var log = require('debug')('sen:models:image')
var logInfo = require('debug')('sen:models:image:info')

const ignoredDomains = ['insightexpressai']

const ignoredDomainsRegex = RegExp(ignoredDomains.join('|'), 'igm')
export class Image extends Model {
  [x: string]: any
}
export function ImageModel() {
  Image.init(
    {
      restAttributes: {
        type: JSONB
      },
      processed: {
        type: BOOLEAN,
        defaultValue: false
      },
      fixed: {
        type: BOOLEAN,
        defaultValue: false
      },
      invalidIptc: {
        type: BOOLEAN,
        defaultValue: false
      },
      size: { type: INTEGER },
      width: { type: INTEGER },
      height: { type: INTEGER },
      density: { type: INTEGER },
      space: { type: STRING },
      channels: { type: INTEGER },
      title: {
        type: TEXT
      },
      src: {
        type: TEXT
      },
      original_src: {
        type: TEXT
      },
      srcSet: {
        type: TEXT
      },
      alt: {
        type: TEXT
      },
      phash: {
        type: TEXT
      },
      src_hash: {
        type: TEXT
      },
      content_hash: {
        type: TEXT
      },
      metadata_hash: {
        type: TEXT
      },
      server: {
        type: TEXT,
        comment:
          'this is server header if exists. useful to see how images are served, cloudinary, imgix etc ...'
      },
      errors: {
        type: ARRAY(TEXT)
      }
    },
    {
      sequelize: c,
      modelName: 'images',
      timestamps: true,
      underscored: true,
      paranoid: true
    }
  )
  Image.belongsTo(UrlModel())
  return Image
}

export async function syncImagesBulk(images, urlId) {
  const d = images
    .map(i => {
      const {
        attribs: { alt, src, srcset: srcSet, sizes, ...restAttributes }
      } = i
      if (!src) {
        return null
      }
      // no idea why i need to do this twice :D
      if (ignoredDomainsRegex.test(src)) {
        log(`Skipping url because it is in ignored list`)
        return null
      }

      if (ignoredDomainsRegex.test(src)) {
        log(`Skipping url because it is in ignored list`)
        return null
      }
      return {
        alt,
        src: fixUrl(src),
        srcSet,
        sizes,
        restAttributes,
        urlId
      }
    })
    .filter(Boolean)

  await ImageModel().bulkCreate(d)
}
