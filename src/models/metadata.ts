import { Model, TEXT, INTEGER } from 'sequelize'

import { c } from '../utils/db'
import { flattenObject, isEmptyObject } from '../utils/utils'
import { ImageModel } from './image'

var log = require('debug')('sen:models:metadata')
var logInfo = require('debug')('sen:models:metadata:info')

export class Metadata extends Model {}
export function MetadataModel() {
  Metadata.init(
    {
      uri: {
        type: TEXT,
        comment: `xmp:copyright,
        xmp:raw:[1]:foo
        xmp:raw:foo:foo`
      },
      value: {
        type: TEXT
      },
      namespace: {
        type: TEXT
      },
      image_id: {
        type: INTEGER,
        references: {
          model: ImageModel(),
          key: 'id'
        }
      }
    },
    {
      sequelize: c,
      modelName: 'metadata',
      timestamps: true,
      underscored: true,
      indexes: [
        {
          fields: ['image_id', 'uri', 'value'],
          unique: true
        },
        {
          fields: ['namespace']
        }
      ]
    }
  )

  return Metadata
}

export function generateMetadataTableBulkInsertRecords(
  metadata: { [k: string]: any },
  image_id: number
): object[] {
  let metadataList: object[] = []

  const imageIdTemplate = { image_id }

  let exif = flattenObject({ exif: metadata.exif })
  let iptc = flattenObject({ iptc: metadata.iptc })
  let icc = flattenObject({ icc: metadata.icc })
  let xmp = flattenObject({ xmp: metadata.xmp })

  if (!isEmptyObject(exif)) {
    // log(`Processing exif for image ${image_id} ${Object.keys(exif).length}`)
    const exifList = Object.keys(exif).map(x => {
      const val = exif[x]
      return {
        uri: x,
        value: val,
        ...imageIdTemplate,
        namespace: 'exif'
      }
    })

    metadataList.push(...exifList)
  }

  if (!isEmptyObject(iptc)) {
    // log(`Processing iptc for image ${image_id} ${Object.keys(iptc).length}`)
    const iptcList = Object.keys(iptc).map(x => {
      const val = iptc[x]
      return {
        uri: x,
        value: val,
        ...imageIdTemplate,
        namespace: 'iptc'
      }
    })
    metadataList.push(...iptcList)
  }

  if (!isEmptyObject(icc)) {
    // log(`Processing icc for image ${image_id} ${Object.keys(icc).length}`)

    const iccList = Object.keys(icc).map(x => {
      const val = icc[x]
      return {
        uri: x,
        value: val,
        ...imageIdTemplate,
        namespace: 'icc'
      }
    })
    metadataList.push(...iccList)
  }

  if (!isEmptyObject(xmp)) {
    // log(`Processing xmp for image ${image_id} ${Object.keys(xmp).length}`)
    const xmpList = Object.keys(xmp).map(x => {
      const val = xmp[x]
      return {
        uri: x,
        value: val,
        ...imageIdTemplate,
        namespace: 'xmp'
      }
    })
    metadataList.push(...xmpList)
  }
  return metadataList
}

export async function migrateMetadata(): Promise<any> {
  // console.error('not needed')
  const [metadata] = await c.query(
    `SELECT * FROM public.metadata WHERE  namespace IS null;`
  )
  let metadataList: object[] = []

  metadata.map(async (m: any) => {
    const namespace = m.uri.split(':')[0]

    metadataList.push({ ...m, namespace })
  })

  return await MetadataModel()
    .bulkCreate(metadataList, {
      updateOnDuplicate: ['namespace']
    })
    .catch(e => {
      return Promise.reject(e)
    })
}
