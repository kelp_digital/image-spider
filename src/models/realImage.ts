import { STRING, JSON, BOOLEAN, TEXT, INTEGER, ARRAY } from 'sequelize'

import { Image } from './image'
import { c } from '../utils/db'
var log = require('debug')('sen:models:realImage')
var logInfo = require('debug')('sen:models:realImage:info')

export class RealImage extends Image {}
export function RealImageModel() {
  RealImage.init(
    {
      iptc: {
        type: JSON
      },
      icc: {
        type: JSON
      },
      exif: {
        type: JSON
      },
      xmp: {
        type: JSON
      },
      restAttributes: {
        type: JSON
      },
      processed: {
        type: BOOLEAN,
        defaultValue: false
      },
      invalidIptc: {
        type: BOOLEAN,
        defaultValue: false
      },
      size: { type: INTEGER },
      width: { type: INTEGER },
      height: { type: INTEGER },
      density: { type: INTEGER },
      space: { type: STRING },
      channels: { type: INTEGER },
      title: {
        type: TEXT
      },
      src: {
        type: TEXT
      },
      original_src: {
        type: TEXT
      },
      srcSet: {
        type: TEXT
      },
      alt: {
        type: TEXT
      },
      phash: {
        type: TEXT
      },
      src_hash: {
        type: TEXT
      },
      content_hash: {
        type: TEXT
      },
      metadata_hash: {
        type: TEXT
      },
      server: {
        type: TEXT,
        comment:
          'this is server header if exists. useful to see how images are served, cloudinary, imgix etc ...'
      },
      errors: {
        type: ARRAY(TEXT)
      }
    },
    {
      sequelize: c,
      modelName: 'images',
      timestamps: true,
      underscored: true
    }
  )
  return RealImage
}
