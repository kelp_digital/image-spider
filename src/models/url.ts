import { Model, STRING, BOOLEAN, ARRAY, TEXT } from 'sequelize'
import { c } from '../utils/db'
import { makeHash } from '../utils/utils'

export class Url extends Model {}

export function UrlModel() {
  Url.init(
    {
      url: {
        type: STRING,
        unique: true
      },
      urlHash: {
        type: STRING,
        unique: true
      },
      processed: {
        type: BOOLEAN,
        defaultValue: false
      },
      marketType: {
        type: STRING
      },
      errors: {
        type: ARRAY(TEXT)
      }
    },
    {
      sequelize: c,
      modelName: 'urls',
      timestamps: true,
      underscored: true
    }
  )
  return Url
}

export async function addUrl({ url, marketType = '' }): Promise<Url> {
  return await UrlModel().findOrCreate({
    where: {
      urlHash: await makeHash(url.trim())
    },
    defaults: {
      url: url.trim(),
      marketType
    }
  })
}
