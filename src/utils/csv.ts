import { readFileSync, existsSync } from 'fs'
import { resolve, relative } from 'path'
var log = require('debug')('sen:csv')
var logInfo = require('debug')('sen:csv:info')
var logError = require('debug')('sen:csv:error')

import parse from 'csv-parse/lib/sync'
import { fixUrl, makeHash, elapsed_time } from './utils'

import { URL } from 'url'
import { UrlModel } from '../models/url'
import { Op } from 'sequelize'

export async function prepareCSV(path: string): Promise<string[][]> {
  const file = readFileSync(path)

  const records = parse(file, {
    skip_empty_lines: true,
    trim: true
  })

  return records.map(v => {
    const url = new URL(fixUrl(v[0].trim()))
    const type = v[1].trim()
    return [url.toString(), type]
  })
}

export function fileExists(path: string): boolean {
  const realPath = resolve(__dirname, '../../', path)

  return existsSync(realPath)
}

export function parseFile(file: string): string {
  if (!fileExists(file)) {
    throw new Error('File does not exist')
  }

  const realPath = resolve(__dirname, '../../', file)
  return realPath
}

export async function readParseAndSync(file: string): Promise<any> {
  const urls = await prepareCSV(parseFile(file))
  logInfo(`Found ${urls.length} urls to process`)
  return await syncCSVData(urls)
}

export async function syncCSVData(urls: string[][]) {
  const start = process.hrtime()
  const bulkInsert: any = await Promise.all(
    urls.map(async url => {
      return {
        url: url[0].trim(),
        urlHash: await makeHash(url[0].trim()),
        marketType: url[1].trim()
      }
    })
  ).catch(error => {
    logError(error.message)
  })
  await fixAllMarketType(bulkInsert)
  await UrlModel().bulkCreate(bulkInsert, {
    // updateOnDuplicate: ['market_type', 'url', 'url_hash']
    ignoreDuplicates: true
  })
  log(`DONE, elapsed time ${elapsed_time(start)}`)
}

export async function fixMarketType(url: string, marketType: string) {
  const list = await UrlModel().findAll({
    // limit: 1,
    where: {
      url: { [Op.like]: `%${url}%` },
      marketType: null
    }
  })

  return Promise.all(
    list.map(async l => {
      log(`processing ${l.get('id')}`)
      return await l.update({ marketType })
    })
  )
}
export async function fixAllMarketType(bulkInsert: any) {
  return Promise.all(
    bulkInsert.map(l => {
      return fixMarketType(l.url, l.marketType)
    })
  )
}
