import { Sequelize } from 'sequelize'
import { UrlModel } from '../models/url'
import { ImageModel } from '../models/image'
import { RealImageModel } from '../models/realImage'
import { MetadataModel } from '../models/metadata'

const log = require('debug')('sen:db')

const { DB_URI } = process.env

// Cached connection
export let c: Sequelize

/**
 * Setup Db and connection
 * @param {*} params
 */
export async function dbSetup(force = false) {
  await db()

  UrlModel()
  ImageModel()
  RealImageModel()
  MetadataModel()
  const res = await c.sync()
  // log(res)
}

/**
 * get cached connection
 */
export function db(): Sequelize {
  if (!c) {
    log('creating db instance')
    c = new Sequelize(
      DB_URI || 'postgres://admin:admin@localhost:54321/spider',
      {
        logging: false
      }
    )
  }
  return c
}
