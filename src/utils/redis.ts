import Redis from 'ioredis'

export const defaultRedisUri = 'redis://127.0.0.1:6379'

const { REDIS_URI } = process.env

export let redisClient

export async function setupRedis() {
  if (!redisClient) {
    redisClient = new Redis(REDIS_URI || defaultRedisUri)
  }
}
export function toKey(key: string, prefix: string) {
  return [prefix, key].join(':')
}

export async function setToCache(
  key: string,
  value: any,
  prefix = 'webcrawler'
) {
  return await redisClient.set(toKey(key, prefix), value)
}

export async function getFromCache(key: string, prefix = 'webcrawler') {
  return await redisClient.get(toKey(key, prefix))
}
