import { fixUrl, fixLstSlashUrl } from './utils'

describe('utils', () => {
  test('fixUrl', () => {
    expect(fixUrl('sensio.group')).toBe('https://sensio.group')
    expect(fixUrl('//sensio.group')).toBe('https://sensio.group')
    expect(fixUrl('https://sensio.group')).toBe('https://sensio.group')
    expect(fixUrl('http://sensio.group')).toBe('http://sensio.group')
    expect(fixUrl('/assets/images/ap.svg')).toBe('/assets/images/ap.svg')
    expect(
      fixUrl(
        'data:image/gif;base64,r0lgodlhaqabaiaaaaaaap///yh5baeaaaaalaaaaaabaaeaaaibraa7'
      )
    ).toBe(
      'data:image/gif;base64,r0lgodlhaqabaiaaaaaaap///yh5baeaaaaalaaaaaabaaeaaaibraa7'
    )
  })
  test('fixLstSlashUrl', () => {
    expect(fixLstSlashUrl('//sensio.group')).toBe('//sensio.group')
    expect(fixLstSlashUrl('//sensio.group/')).toBe('//sensio.group')
  })
})
