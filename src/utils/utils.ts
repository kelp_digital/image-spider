import { isURL, isDataURI } from 'validator'
import multihashing from 'multihashing-async'
import { extname } from 'path'

export function fixUrl(s: string, https = true): string {
  let uri = s
  const protocol = https ? 'https' : 'http'
  if (!isURL(uri) && !isDataURI(uri)) {
    if (uri.startsWith('//')) {
      uri = `${protocol}:${uri}`
    } else {
      if (!uri.startsWith('/')) {
        uri = `${protocol}://${uri}`
      }
    }
  } else {
    if (!uri.startsWith('http') && !isDataURI(uri)) {
      uri = `${protocol}://${uri}`
    }
  }

  return uri
}

export function fixLstSlashUrl(s: string): string {
  let url = s.toLocaleLowerCase()

  if (url.slice(-1) === '/') {
    url = url.slice(0, -1)
  }
  if (url.slice(-1) === '?') {
    url = url.slice(0, -1)
  }
  return url
}

export async function makeHash(str: string, algo = 'sha2-256') {
  const hash = await multihashing(Buffer.from(str.trim()), algo)
  return hash.toString('hex')
}

export function hasExtension(str: string): boolean {
  return !!extname(str).substr(1)
}

// https://gist.github.com/penguinboy/762197
export function flattenObject(ob: object, delimiter = ':'): object {
  let toReturn = {}

  for (let i in ob) {
    if (!ob.hasOwnProperty(i)) continue
    // this is buffer in its string form
    if (i === 'type' || i === 'data') {
      console.log(`skipped the record because it's a buffer... @TODO`)
      continue
    } else {
      if (typeof ob[i] == 'object') {
        let flatObject = flattenObject(ob[i])
        for (let x in flatObject) {
          if (!flatObject.hasOwnProperty(x)) continue
          toReturn[i + delimiter + x] = flatObject[x]
        }
      } else {
        const v = ob[i].toString().trim()
        if (!v) continue

        toReturn[i] = v
      }
    }
  }

  return toReturn
}

export function isEmptyObject(obj: object): boolean {
  return Object.keys(obj).length === 0
}
/**
 * elapsed time
 * @param start
 * @return miliseconds
 */
export const elapsed_time = (start, withTheEnd = true): string => {
  const elapsed = process.hrtime(start)[1] / 1000000 // divide by a million to get nano to milli
  const e = elapsed.toFixed(3)
  return withTheEnd ? `${e} in miliseconds` : e
}
