select
	count(*) as vals,
	u.url as lbls
from
	public.images as i
join public.urls as u on
	u.id = i.url_id
group by i.url_id, u.url
order by vals DESC
;
